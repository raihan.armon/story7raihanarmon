
from django.test import TestCase, Client
from django.urls import resolve

from .views import index

# Create your tests here.
class UnitTest(TestCase):
    def test_url_exist(self):
        response = Client().get('//')
        self.assertEqual(response.status_code, 200)

    def test_correct_template_used(self):
        response = Client().get('//')
        self.assertTemplateUsed(response, 'index.html')

    # def test_correct_views_used(self):
    #     found = resolve('//')
    #     self.assertEqual(found.func, index)

