from django.urls import path
from .views import index

app_name = 'story7th'

urlpatterns = [
    path('', index, name='index'),
    
]